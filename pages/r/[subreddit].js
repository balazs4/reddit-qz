import { useState, useEffect } from 'react';
import Head from 'next/head';
import fetch from 'isomorphic-fetch';
import Score from '../../components/score';
import Item from '../../components/item';

const Subreddit = ({ items }) => {
  const [user, setUser] = useState(undefined);
  const [tmp, setTmp] = useState(user);
  const [scores, setScores] = useState([]);
  const [index, setIndex] = useState(0);

  useEffect(() => {
    const name = window.localStorage.getItem('reddit-qz');
    if (name) {
      setUser(name);
    } else {
      setUser(null);
    }
  }, []);

  const vote = item => title => e => {
    e.preventDefault();
    setScores([...scores, item.title === title ? 1 : 0]);
    setIndex(index + 1);
  };

  if (user === undefined) {
    return null;
  }

  if (user === null) {
    return (
      <>
        <input
          type="text"
          placeholder="Enter your nickname"
          value={tmp}
          onChange={e => {
            if (/^\s/.test(e.target.value)) return;
            setTmp(e.target.value);
          }}
        />
        <button
          type="button"
          onClick={e => {
            e.preventDefault();
            window.localStorage.setItem('reddit-qz', tmp);
            setUser(tmp);
          }}
        >
          GO
        </button>
      </>
    );
  }

  return (
    <>
      <Head>
        <title>reddit-qz</title>
      </Head>
      <h2>Hey, {user}</h2>
      {items.map((item, i) => (
        <div key={item.id} style={{ display: i === index ? 'block' : 'none' }}>
          <Item
            {...item}
            vote={vote(item)}
            status={`${index + 1} of ${items.length}`}
          />
        </div>
      ))}
      {index >= items.length && <Score scores={scores} user={user} />}
      <style jsx global>
        {`
          html {
            overflow: scroll;
            overflow-y: always;
          }
          body {
            font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI',
              Helvetica, Arial, sans-serif;
            line-height: 1.6;
            color: #222;
            max-width: 40rem;
            padding: 2rem;
            margin: auto;
            background: #fafafa;
          }
          img {
            width: 100%;
          }
          a {
            color: #2ecc40;
          }
          h1,
          h2,
          strong {
            color: #111;
          }
        `}
      </style>
    </>
  );
};

Subreddit.getInitialProps = async ({ query }) => {
  const items = await getItems(query.subreddit, query.count);
  return { items };
};

export default Subreddit;

const getItems = (subreddit, count = 10) =>
  fetch(`https://api.reddit.com/r/${subreddit}/hot?t=month&limit=100`)
    .then(x => x.json())
    .then(response =>
      (response && response.data && response.data.children
        ? response.data.children
        : []
      )
        .map(x => x.data)
        .filter(x => x.domain === 'i.redd.it')
        .map(x => ({ id: x.id, title: x.title, url: x.url }))
    )
    .then(x => {
      x.sort(() => (Math.random() > Math.random() ? 1 : -1));
      return x;
    })
    .then(x =>
      x.map((y, i) => {
        let random = i;
        do {
          random = Math.ceil(Math.random() * 100) % x.length;
        } while (random === i);
        const fake = x[random];
        return { ...y, fake };
      })
    )
    .then(x => x.slice(0, count));
