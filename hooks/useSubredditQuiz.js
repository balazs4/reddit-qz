import { useEffect, useState } from 'react';

export default (subreddit, count = 10) => {
  const [items, setItems] = useState(null);
  useEffect(() => {
    fetch(`https://api.reddit.com/r/${subreddit}/hot?t=month&limit=100`)
      .then(x => x.json())
      .then(response =>
        (response && response.data && response.data.children
          ? response.data.children
          : []
        )
          .map(x => x.data)
          .filter(x => x.domain === 'i.redd.it')
          .map(x => ({ id: x.id, title: x.title, url: x.url }))
      )
      .then(x => {
        x.sort(() => (Math.random() > Math.random() ? 1 : -1));
        return x;
      })
      .then(x =>
        x.map((y, i) => {
          let random = i;
          do {
            random = Math.ceil(Math.random() * 100) % x.length;
          } while (random === i);
          const fake = x[random];
          return { ...y, fake };
        })
      )
      .then(x => x.slice(0, count))
      .then(x => setItems(x));
  }, [subreddit, count]);

  return [items];
};
