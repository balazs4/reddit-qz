const Score = props => {
  const total = props.scores.length;
  const your = props.scores.filter(x => x === 1).length;

  return (
    <>
      <p>
        🏅 {props.user}'s score:
        <span>
          {your} of {total}
        </span>
      </p>
      <style jsx>
        {`
          p {
            font-size: 1.6rem;
            display: flex;
            flex-direction: row;
            justify-content: center;
            align-items: center;
            height: 80vh;
          }
          span {
            font-weight: bolder;
            margin-left: 8px;
          }
        `}
      </style>
    </>
  );
};

export default Score;
