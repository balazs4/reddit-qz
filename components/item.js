const VoteButton = props => (
  <>
    <button type="button" onClick={props.vote(props.children)}>
      {props.children}
    </button>
    <style jsx>
      {`
        button {
          flex: 1;
          background-color: #fff;
          border: solid 1px gray;
          color: #888;
          padding: 15px 32px;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          font-size: 16px;
        }
      `}
    </style>
  </>
);

const Item = props => {
  const flop = Math.ceil(Math.random() * 100) % 2 === 1;

  return (
    <>
      <p>
        🕵️ What do you see? <span>{props.status}</span>
      </p>
      <div>
        <VoteButton vote={props.vote}>{props.title}</VoteButton>
        <VoteButton vote={props.vote}>{props.fake.title}</VoteButton>
      </div>
      <img src={props.url} alt={props.id} />
      <style jsx>
        {`
          div {
            display: flex;
            flex-direction: ${flop ? 'row' : 'row-reverse'};
            justify-content: space-around;
          }
          p {
            font-size: 1.3rem;
          }
          span {
            color: #999;
            font-size: 1rem;
            text-align: right;
          }
        `}
      </style>
    </>
  );
};

export default Item;
